# to create a list of the QUANDLcommands used to import forward exchange rates for different expiration dates

# first I do for GBP
import os

os.system('sed -i "s/BPZ/BPH/g" /home/gio/Desktop/currency_fw/bph')
for i in range(1,39):
    year = 1979 + i
    os.system('sed -i "'+ str(i) +'s/1980/'+ str(year)+'/g" /home/gio/Desktop/currency_fw/bph')

os.system('sed -i "s/BPZ/BPM/g" /home/gio/Desktop/currency_fw/bpm')    
for i in range(1,39):
    year = 1979 + i
    os.system('sed -i "'+ str(i) +'s/1980/'+ str(year)+'/g" /home/gio/Desktop/currency_fw/bpm')

os.system('sed -i "s/BPZ/BPU/g" /home/gio/Desktop/currency_fw/bpu')
for i in range(1,39):
    year = 1979 + i
    os.system('sed -i "'+ str(i) +'s/1980/'+ str(year)+'/g" /home/gio/Desktop/currency_fw/bpu')

for i in range(1,39):
    year = 1979 + i
    os.system('sed -i "'+ str(i) +'s/1980/'+ str(year)+'/g" /home/gio/Desktop/currency_fw/bpz')


# Then I paste all the results in a single file called GBP and I create a copy called EUR
os.system('cat bph > /home/gio/Desktop/currency_fw/GBP')
# remember to sub > to >> from the second for not overwritting previous content
os.system('cat bpm >> /home/gio/Desktop/currency_fw/GBP')
os.system('cat bpu >> /home/gio/Desktop/currency_fw/GBP')
os.system('cat bpz >> /home/gio/Desktop/currency_fw/GBP')

# here I create the copy
os.system('cat GBP > /home/gio/Desktop/currency_fw/EUR')
# and I substitute BP* with EC*
os.system('sed -i "s/BPH/ECH/g" /home/gio/Desktop/currency_fw/EUR')
os.system('sed -i "s/BPM/ECM/g" /home/gio/Desktop/currency_fw/EUR')
os.system('sed -i "s/BPU/ECU/g" /home/gio/Desktop/currency_fw/EUR')
os.system('sed -i "s/BPZ/ECZ/g" /home/gio/Desktop/currency_fw/EUR')
