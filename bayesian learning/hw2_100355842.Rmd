---
title: "HW2 option A"
author: "Giovanni Bignardi 100355842"
date: "March 11, 2017"
output: html_document
---
Datasets from: https://www.kaggle.com/wendykan/lending-club-loan-data

These files contain complete loan data for all loans issued through the 2007-2015, including the current loan status (Current, Late, Fully Paid, etc.) and latest payment information. The file containing loan data through the "present" contains complete loan data for all loans issued through the previous completed calendar quarter. Additional features include credit scores, number of finance inquiries, address including zip codes, and state, and collections among others. The file is a matrix of about 890 thousand observations and 75 variables. A data dictionary is provided in a separate file.

```{r, comment=NA, include=FALSE}
library('ff') 
```

The idea was to demonstrate that bad loans aren't necessarily correlated with the highest interest rates.
```{r, comment=NA}
loan = read.csv('loan.csv', header = TRUE)
dim(loan)
names(loan)
head(loan)
```

I need to prepare the data: delete on-going loans and discerne between 'Good' loans (i.e. repaid) and 'Bad' loans.
```{r, comment=NA}
# Mark
good_status = c('Fully Paid')
on_going_status = c('Issued','Current')
loan$mark = ifelse(loan$loan_status %in% good_status,"Good",
                           ifelse(loan$loan_status %in% on_going_status,"On going","Bad"))
loan$mark = factor(loan$mark)

loan_red = subset(loan, mark == "Good" | mark == "Bad") # remove on-going loans

loan_bad = subset(loan_red, mark=='Bad')

ir = na.omit(loan_bad$int_rate)

ir_train = ir[1:(0.75*length(ir))]
ir_test = ir[((0.75*length(ir))+1):length(ir)]

rm(loan)
```

The idea is to demonstrate that bad loans aren't necessarily correlated with the highest interest rates; then let's try to see how the original data behave.
I compute an empirical cumulative distribution function for ir_train and plot it:
```{r, comment=NA}
F = ecdf(ir_train)
plot(F)
```

79% of bad loans had an int_rate above 10%:
```{r, comment=NA}
1-F(10)
```

But only 10.9% had an interest rate above 20%:
```{r, comment=NA}
(1-F(20))/(1-F(10))
```

I plot the data to look at their distribution:
```{r, comment=NA}
hist(ir_train)
mean = mean(ir_train)
```

The distribution they recall the most is a normal distribution: $ir \sim N(\alpha,\sigma^2)$.

With known mean I can assume that the $\sigma^2$ (variance) of the distribution follows a conjugate prior distribution of type inverse gamma, i.e.: $\sigma^2 \sim InverseGamma(\alpha, \beta)$.
The posterior hyperparameters of such distribution are:

+ $\alpha_{post}=\alpha_0+n/2$, where n is the number of instance in my data and 

+ $\beta_{post}=\beta_0+\frac{\sum_{i=1}^{n}(x_{i}-\mu)^2}{2}$

Then the posterior predictive distribution would be a Normal of type 

$N(\mu,\sigma^2_{post})$ where $\sigma^2_{post} \sim InverseGamma(\alpha_{post},\beta_{post})$.

I set the prior hyperparameters for the inverse gamma: a_prior and b_prior equal to zero.
```{r, comment=NA}
# MODEL 1 , 
# likelihood is normal with known mean
# prior for variance is inverse gamma

a_prior = 0
b_prior = 0

a_post = a_prior + length(ir_train)

aux = c()
for(i in 1:length(ir_train)){
  aux[i] = (ir_train[i]-mean)^2
}
b_post = b_prior + sum(aux)/2

sigma2_distr = 1/rgamma(n = length(ir_train), shape = a_post, rate = b_post)

ir_train.post = rnorm(n = length(ir_train), mean = mean, sd = sigma2_distr) 

#FAILS
cat('train freq:',sum(ir_train<12)/52062, '\npost prediction:',sum(ir_train.post<12)/52062,'\ntest freq',sum(ir_test<12)/length(ir_test))

cat('train freq:',sum(ir_train<20)/52062, '\npost prediction:',sum(ir_train.post<20)/52062,'\ntest freq',sum(ir_test<20)/length(ir_test))

cat('train freq:',sum(ir_train>16)/52062, '\npost prediction:',sum(ir_train.post>16)/52062,'\ntest freq',sum(ir_test>16)/length(ir_test))
```

The model is quite accurate in predicting the frequency below the mean but fails elsewhere, due to an over-stretched posterior variance. 

The solution I try it's to assume known variance for the distribution and let the mean be unknown instead.
With known variance I can assume that the $\mu$ (mean) of the distribution follows a conjugate prior distribution of type normal, i.e.: $\mu \sim N(\mu_{0}, \sigma^2_{0})$.
The posterior hyperparameters of such distribution are:

+ $\mu'_{post}=(\frac{\mu_0}{\sigma^2_0}+\frac{\sum_{i=1}^{n}x_{i}}{\sigma^2})/(\frac{1}{\sigma^2_0}+\frac{n}{\sigma^2})$ and 

+ $\sigma_{post}^2=1/(\frac{1}{\sigma^2_0}+\frac{n}{\sigma^2})$

Then the posterior predictive distribution would be a Normal of type $N(\mu_{post},\sigma^2)$ where $\mu_{post} \sim N(\mu'_{post},\sigma^2_{post})$

```{r, comment=NA}
##################
##### MODEL 2,  
# likelihood is normal with known variance
# prior for mean is normal

n = length(ir_train)
sigma2 = sd(ir_train)^2
m.prior = 0
v.prior = 1 # I set the prior variance for the mean distribution equal to 1.

m.post = (m.prior/v.prior + sum(ir_train)/sigma2)/(1/v.prior + n/sigma2)
v.post = 1/(1/v.prior + n/sigma2)

m.post_dist = rnorm(n = length(ir_train), mean = m.post, sd = v.post)
ir_train.post2 = rnorm(n = length(ir_train), mean = m.post_dist, sd = sqrt(sigma2)) 

# BEST!
cat('train freq:',sum(ir_train<12)/52062, '\npost prediction:',sum(ir_train.post2<12)/52062,'\ntest freq',sum(ir_test<12)/length(ir_test))

cat('train freq:',sum(ir_train<20)/52062, '\npost prediction:',sum(ir_train.post2<20)/52062,'\ntest freq',sum(ir_test<20)/length(ir_test))

cat('train freq:',sum(ir_train>16)/52062, '\npost prediction:',sum(ir_train.post2>16)/52062,'\ntest freq',sum(ir_test>16)/length(ir_test))
```

These were the best results I get.

However,the fact that my original distribution resemble a normal is questionable, then, I've tried to assume also another parametric model, a gamma, with a gamma conjugate prior, to check if it could perform better.

The results aren't neither bad nor exciting:
```{r}
# MODEL 3
# Gamma(a,b) with known a (shape)
b = mean(ir_train)/sd(ir_train)
a.shape = mean(ir_train)*b

a.prior = 0
b.prior = 0

a.post = a.prior + length(ir_train)*a.shape
b.post = b.prior + sum(ir_train)

b.dist = rgamma(n = length(ir_train), shape = a.post, rate = b.post)

ir_train.post3 = as.data.frame(rgamma(n = length(ir_train), shape = a.shape, rate = b.dist))

cat('train freq:',sum(ir_train<12)/52062, '\npost prediction:',sum(ir_train.post3<12)/52062,'\ntest freq',sum(ir_test<12)/length(ir_test))

cat('train freq:',sum(ir_train<20)/52062, '\npost prediction:',sum(ir_train.post3<20)/52062,'\ntest freq',sum(ir_test<20)/length(ir_test))

cat('train freq:',sum(ir_train>16)/52062, '\npost prediction:',sum(ir_train.post3>16)/52062,'\ntest freq',sum(ir_test>16)/length(ir_test))
```

***

Another path I could have followed to solve, in a certain way more precise. Is to assume a normal parametric model with a normal-inverse gamma prior for $\mu$ and $\sigma^2$, assuming exchangeability; then obtain the posterior normal distribution and use it to get a t-student predictive density (this last step was skipped previously):
```{r, comment=NA}
m=0; c=0.01; a=0.01; b=0.01;

n=length(ir_train)
mean.ir_train=mean(ir_train)
var.ir_train=var(ir_train)

m.ast=(c*m+n*mean.ir_train)/(c+n)
c.ast=c+n
a.ast=a+n
b.ast=b+(n-1)*var.ir_train+c*n*(m-mean.ir_train)^2/(c+n)
```

The probability that a non-repaid loan will have an interest rate >10%:
```{r, comment=NA}
(P10 = 1-pt((10-m.ast)/sqrt((c.ast+1)*b.ast/(c.ast*a.ast)),a.ast))
```
The probability that a non-repaid loan will have an interest rate >10% is `r P10`, which is just slightly different from the real value!
```{r, comment=NA}
length(subset(ir_test, ir_test > 10))/length(ir_test)
```

***

The probability that a non-repaid loan will have an interest rate above 20%
```{r, comment=NA}
(P20=(1-pt((20-m.ast)/sqrt((c.ast+1)*b.ast/(c.ast*a.ast)),a.ast))/P10)
```
The probability that a non-repaid loan will have an interest rate above 20% is `r P20`. Which is quite near to the real value! In fact:
```{r, comment=NA}
length(subset(ir_test, ir_test > 20))/length(ir_test)
```

The best thing would be to keep this last model because it adapts both the mean and the variance and I don't have to control by fixing one of them.
```{r, comment=NA}

```
