---
title: '__Practice Workbook__: Chapter 2 *Multiple Linear Regression*'
output: html_document
author: Giovanni Bignardi 100355842
---

## [1] cost.xlsx
*The file cost.xlsx contains n = 127 observations on the variables unit cost of production; salary per hour; energy cost; cost of raw materials; and depreciation of machinery and equipment. These data are collected in different factories of a determined company. The response variable is the cost of production, that is to be explained as a function of the other four regressor variables.*


First I set the working directory and using library openxlsx I load the dataset
```{r comment=NA}
library(openxlsx)
setwd('~/Dropbox/predictive modelling/proj done/')
cst = read.xlsx('cost.xlsx') #my data frame

```
***
####a) **Make an initial exploratory plot of the data.**
```{r comment=NA}

#x11();

plot(cst, col = 'red', pch = 16, 
     cex = 1.1, cex.labels = 1.3, 
     font.labels = 2, gap = 0.15,
     main = 'cst (cost.xlsx)'); 

```

In the plot the relations between the variables isn't clear, I can see different distributions with many outliers. I take a logarithmic transformation of both the dependent and the independent variables to see if in such way the data display some more relation.

```{r comment=NA}
plot(log(cst), col = 'blue', pch = 16, 
     cex = 1.1, cex.labels = 1.3, 
     font.labels = 2, gap = 0.15, 
     labels = paste0('log(',colnames(cst),')'), #to change the variables' labels in the plot, inserting each one into a log()
     main = 'log(cst variables)')
```

The logarithmic transformation strengthly improve the data appearence: I can clearly see some linear relations, and, even if some scatterplots still show several outlier records, I can say to be quite sure that the regression will yield a better prediction if I fit the model on the transformed variables.

***
####b) **Fit a multiple linear regression model including all the regressors**

First I try to fit a model without transforming the variables
```{r comment=NA}
f = cost ~ .; # For reducing notation when the number of regressors is large

model.1 = lm(f, data = cst); 

# With multiple regressions I prefer to keep significance stars when visualizing the summary 

round(summary(model.1)$coefficients, d=4);
summary(model.1)$r.squared;
summary(model.1)$sigma

```

This model doesn't seem to perform bad but the residual standard error is very high, confronted with the coefficient average magnitude. Moreover the previous analysis on the data plots revealed many outliers and no apparent signs of linear relations between the main part of the variables.

I should apply a log transformation of the variables and see if the model effectively performs better.
Thus, I'm going to use a MULTIPLICATIVE MODEL of type 

$\hat{y}=e^{\hat{\beta_{0}}}x_{1}^{\hat{\beta_{1}}}x_{2}^{\hat{\beta_{2}}}x_{3}^{\hat{\beta_{3}}}x_{4}^{\hat{\beta_{4}}}$

in our case

$\hat{cost} = e^{\beta_{intercept}}x_{salary}^{\hat{\beta_{salary}}}x_{raw}^{\hat{\beta_{raw}}}x_{energy}^{\hat{\beta_{energy}}}x_{machinery}^{\hat{\beta_{machinery}}}$

NOTE: I'm using log with base *e* (where *e* <- exp(1) = `r exp(1)`) for transformation. If I used log10 the relation would have been of type $\hat{y}=10^{\hat{\beta_{0}}}x_{1}^{\hat{\beta_{1}}}x_{2}^{\hat{\beta_{2}}}...$

{From where does it come from?}

When you use a log transformation **on the response**, the regression coefficients have a particular interpretation:

$log{\hat{y}}=\hat{\beta_{0}}+\hat{\beta_{1}}x_{1}+\hat{\beta_{2}}x_{2}+\hat{\beta_{3}}x_{3}+\hat{\beta_{4}}x_{4}$

which is equivalent to:

${\hat{y}}=e^{\hat{\beta_{0}}}e^{\hat{\beta_{1}}x_{1}}e^{\hat{\beta_{2}}x_{2}}e^{\hat{\beta_{3}}x_{3}}e^{\hat{\beta_{4}}x_{4}}$

And an increase of one in $x_{1}$ would multiply the predicted response (in the original scale) by $e^{\beta_{1}}$

But I've taken a *log-log* transformation to the model variables. This means that I'm taking the logarithm of **both** *the response* **and** *the predictors*; thus:

$log{\hat{y}}=\hat{\beta_{0}}+\hat{\beta_{1}}log{x_{1}}+\hat{\beta_{2}}log{x_{2}}+\hat{\beta_{3}}log{x_{3}}+\hat{\beta_{4}}log{x_{4}}$

which therefore leads to

$\hat{y}=e^{\hat{\beta_{0}}}e^{\hat{\beta_{1}}log{x_{1}}}e^{\hat{\beta_{2}}log{x_{2}}}e^{\hat{\beta_{3}}log{x_{3}}}e^{\hat{\beta_{4}}log{x_{4}}}$

and given that $e^{\beta*log_{e}{x}}=[(e)^{log_{e}{x}}]^{\beta}=x^{\beta}$

$\hat{y}=e^{\hat{\beta_{0}}}x_{1}^{\hat{\beta_{1}}}x_{2}^{\hat{\beta_{2}}}x_{3}^{\hat{\beta_{3}}}x_{4}^{\hat{\beta_{4}}}$

```{r comment=NA} 
f = log(cost) ~ log(machinery) + log(raw) + 
  log(energy) + log(salary);

model.2 = lm(f, data = cst); 

round(summary(model.2)$coefficients, d=4); 
summary(model.2)$r.squared; 
summary(model.2)$sigma;
```

As a matter of fact the prediction would be better with the logarithmic transformation. It has an higher R^2^ =`r summary(model.2)[8]`, the coefficient are all significants at the maximum grade and the sigma isn't such larger if confronted with the coefficients' magnitude.

***  
####c) **Make a diagnostic plot of (studentized) residuals versus fitted values.** Is there any need of transforming for correcting for some observed anomaly? Which variable is the one that seems to be most responsible for that? Explain
```{r comment=NA}
library(car);
#x11();
residualPlots(model.1, 
              type = 'rstudent',
              tests = FALSE, 
              quadratic = FALSE,
              ask = FALSE, 
              col = 'darkcyan',
              pch = 19,
              cex = 1.5, 
              main = 'St_Residual vs Fitted Values plots (before transforming)'); 


```

The studentized residuals distribution against the fitted values don't behave as a random distribution; the variance of prediction's errors increases and that indicates a problem in the model. 
**Machinery and Salary are actually the two variable which present the widest studentized residuals distributions; they could be the most important variables.**

Following here the studentized residual plot against fitted values of the model with logarithmic transformation:
```{r comment=NA}
#x11()
residualPlots(model.2, 
              type = 'rstudent',
              tests = FALSE, 
              quadratic = FALSE,
              ask = FALSE, 
              col = 'darkred',
              pch = 19,
              cex = 1.5, 
              main = 'St_Residual vs Fitted Values plots'); 
```

***
####d) **After the decision in part (c):** 

+ __Analyze the individual significance of all the regressors.__
+ __Obtain 95% confidence intervals for all the slope parameters.__
+ __What would be the the predicted cost for the values x1(machinery) = 80; x2(raw) = 3000; x3(energy) = 1000; and x4(salary) = 25.__

First I **analyze the individual significance of all the regressors**.\n
When you perform a hypothesis test in statistics, a p-value helps you determine the significance of your results. Hypothesis tests are used to test the validity of a claim that is made about a population. This claim that’s on trial, in essence, is called the null hypothesis.\n
The P-value approach involves determining the probability, assuming the null hypothesis were true, of observing a more extreme test statistic in the direction of the alternative hypothesis, than the one observed. If the P-value is less than the significance level $\alpha$, then the null hypothesis is rejected in favor of the alternative hypothesis (in our case: the regressor is significant).
```{r comment=NA}
cat('',sep = '\n'); format(round(coef(summary(model.2)), d = 4));
```
All the regressors clearly appear to be strongly significant.\n

**Obtain 95% confidence intervals for all the slope parameters.**
```{r comment=NA}
cat('',sep = '\n'); round(confint(model.2, level = .95), d = 4);
```
Since the distribution of the regressors is two-tailed, the 95% confidence intervals are included between two extreme values.

**Predict the cost with x1(machinery) = 80; x2(raw) = 3000; x3(energy) = 1000; and x4(salary) = 25**
```{r comment=NA}
newdata = data.frame(machinery=80, raw=3000, energy=1000, salary=25)

pred_log_cost = as.vector(predict(model.2, newdata, se.fit = TRUE, interval = 'confidence', level = 0.95)[1])
e = exp(1); (pred_cost = e^pred_log_cost$fit[1])
lw = e^pred_log_cost$fit[2]; up = e^pred_log_cost$fit[3]
  
# or I could approximatively compute it manually

(pred_cost2 = (e^0.8539)*(80^0.1219)*(3000^0.1244)*(1000^0.1611)*(25^0.4570))

```
The predicted cost is `r pred_cost`.

With 95% probability the real cost will be `r lw` < real_cost < `r up` 

```{r comment=NA}
# ADDED after class 12/01/2017: I can also use the code of the teacher

newdata = data.frame(80,3000,1000,25); #put the numbers is a way to avoid errors

colnames(newdata) = colnames(cst)[1:4]; 

cat('',sep = '\n'); (pred = exp(predict(model.2, newdata)));

# newdata; cat('',sep = '\n');  tail(cst); then I check if the result obtained is coherent with the original data
```
***
***
***

## [1] cigarettecons.xlsx
*The following questions refer to the data set cigarettecons.xlsx, available in AulaGlobal.
If needed, specify the null and alternative hypothesis. Use the 5% level of significance in
your conclusions.*

```{r comment=NA}

setwd('~/Dropbox/predictive modelling/proj done/')

cig = read.xlsx('cigarettecons.xlsx')

plot(cig[,2:8], col = 'red', pch = 16, 
     cex = 1.1, cex.labels = 1.3, 
     font.labels = 2, gap = 0.15,
     main = 'cigarette consume'); 

```

####a) **Test the hypothesis that the variable Female is not needed in the regression relating Sales to the six regressor variables.**

For this questions a) and b) I will use a simple regression formula relating Sales to the six quantitative variables. **However**, starting from point d) I will propose some exploratory plots of the data and a *log transformation* for the response

The null hypothesis would be $H_{0}: \beta_{Female} = 0$. 

```{r comment=NA}

# State is a cathegorical variable, unfortunately we don't include it in this regression of quantitative variables

f = Sales ~ Age + HS + Income + Black + Female + Price

model = lm(f, data=cig)

summary(model)
```

We can simply observe that the p-value is `r summary(model)$coefficients[6,4]` from the table and conclude that the null cannot be rejected; thus the variable Female is *Non-significant*.

A somewhat more convenient way to compare two nested models is the Analysis of Variance Table.

Set it up the null hypothesis like this:
+ RSSΩ is the RSS for the model with all the predictors of interest (p parameters).
+ RSSω is the RSS for the model with all the above predictors except predictor Female.

I confront the two models:
```{r comment=NA}
f2 = Sales ~ Age + HS + Income + Black + Price #we compute a model without the Female regressor

model_red= lm(f2, data=cig) #or simply model_red = update(moel, .~. - Female)

summary(model_red)

anova(model_red, model)

#or manually compute the F-statistic and the p-value

(F= (sum(model_red$residuals^2)-sum(model$residuals^2))/(sum(model$residuals^2)/44)) #DF=44

p_value = 1-pf(F,1,44)
```

Wait, why Pr(>F)?

Given several predictors for a response, we might wonder whether all are needed. Consider a large model,
Ω, and a smaller model, ω, which consists of a subset of the predictors that are in Ω. By the law of parsimony, we’d prefer to use ω if the data will support it. So we’ll take ω to represent the null hypothesis and Ω to represent the alternative. 
Now suppose that the dimension (number of parameters) of Ω is q and dimension of ω is p. 

The F-statistic can help us $F=\frac{RSS_{omega}-RSS_{\Omega}/(q-p)}{RSS_{\Omega}/(n-q)}$

(I don't include from where it arises)

we would reject the null hypothesis if $F>F^{\alpha}_{q-p,n-q}$ 

R provide a p-value for the F statistic.

When the null is rejected, this does not imply that the alternative model is the best model. We don’t know whether all the predictors are required to predict the response or just some of them. Other predictors might also be added for example quadratic terms in the existing predictors. Either way, the overall F-test is just the beginning of an analysis and not the end.

The null hypotesis can be accepted in our case because from the ANOVA table we can read that Pr($F>F^{\alpha}_{q-p,n-q}$) = 0.8507%

***

####b) **Test the hypothesis that Female and HS are not needed in the above regression equation.**
```{r comment=NA}
red_model = update(model, .~. - Female - HS)

anova(red_model, model)
```
Again we can accept the null hypotesis that Female and HV aren't needed as regressors.

***

####c) **Obtain the 95% confidence interval for the true regression coefficient of the variable Income.**

First I make some analysis of variables to see if I need transformation to fit a good model.
```{r comment=NA}
summary(red_model)

cat('',sep = '\n'); round(confint(red_model, level = .95)[3,0:2], d = 4);
```

***

####d) **What percentage of the total variation in Sales can be accounted for when Income isremoved? Explain.**

First I will try to fit on the data a better performing model.
```{r comment=NA}
# A log-log model doesn't seem appetible
plot(log(cig[,2:8]), col = 'red', pch = 16, 
     cex = 1.1, cex.labels = 1.3, 
     font.labels = 2, gap = 0.15,
     labels = paste0('log(',colnames(cig[,2:8]),')'),
     main = 'log cigarette consume'); 

#Let's try with transforming only the response

logf = log(Sales) ~ . - State

log_model = lm(logf, cig)

summary(log_model)

log_model = update(log_model, .~. - Female - HS - Black )

cig2 = cig
cig2$Sales=log(cig2$Sales)
names(cig2)[8]=paste('log(Sales)')
plot(cig2[,c(2,4,7,8)], col = 'navy', pch = 16, 
     cex = 1.1, cex.labels = 1.3, 
     font.labels = 2, gap = 0.15,
     main = 'log(cigarette consume)'); 

summary(log_model)[c('call','coefficients','adj.r.squared')]
```

This could be a sufficient prediction. Even if the R-squared statistic is really low, it's quite better then the model without transformation.

$R^{2}$, the so-called coefficient of determination or percentage of variance explained, is used to study how well the model fits the data (I've written about the coefficient meaning in the report for simple linear regression).

But **warning!** R-squared measures the proportion of the variation in the dependent variable (Y) explained by the independent variables (X) for a linear regression model.

$R^{2}=\frac{\sum(\hat{y_{i}}-\bar{y_{i}})^{2}}{\sum(y_{i}-\bar{y_{i}})^{2}}=\frac{ESS}{total SS}= 1 - \frac{ESS}{total SS}$

Adjusted R-squared adjusts the statistic based on the **number** of independent variables in the model.

The important difference here is that you can "game" R-squared by adding more and more independent variables, irrespective of how well they are correlated to the dependent variable. Obviously, this isn't a desirable property of a goodness-of-fit statistic. Conversely, adjusted R-squared provides an adjustment to the R-squared statistic such that an independent variable that has a correlation to Y increases adjusted R-squared and any variable without a strong correlation will make adjusted  R-squared decrease. That is the desired property of a goodness-of-fit statistic.

$R_{adj}^{2}=R^{2}-(\frac{p}{n-p-1})(1-R^{2})$

The adjustment is based on the sample size *n*, the number of variables, *p*, and the degree to which the uncorrected model does not fit well $(1− R^{2})$. 

About which one to use...in the case of a linear regression with more than one variable: adjusted R-squared. For a single independent variable model, both statistics are interchangeable.

Removing Income from the predicotrs, the percentage of the total variation in log(Sales) that can be explained by my model is only 19,63%:

```{r comment=NA}
red_mod = update(log_model, .~. - Income)
summary(red_mod)$coefficients
summary(red_mod)$r.squared
summary(red_mod)$adj.r.squared
```

####e) **What percentage of the total variation in Sales can be accounted for by the three variables Price, Age and Income? Explain.**

```{r comment=NA}
(round(summary(log_model)$adj.r.squared, d=4))
```

The three variable can explain 32,53% of the log(Sales) variation.

Without transformation of Sales the percentage explained would have been lower and the coefficients would slightly loose significance. See below:
```{r comment=NA}
modo = update(model, .~. - Female - HS - Black)
summary(modo)[c('coefficients','adj.r.squared')]
```

####f) **What percentage of the total variation in Sales can be accounted for by the variable Income, when Sales is regressed only on this predictor? Explain.**

For a single independent variable model, R-squared and adjusted-R-squared indicators are interchangeable.
```{r comment=NA}
round(summary(lm(log(Sales)~Income, cig))$r.squared, d=4)
```
13,78% of the total variation in Sales can be accounted for by the variable Income, when Sales is regressed only on this predictor

***
***
***

## [3] State.x77 
*Load the data set state.x77, available in R, and consider fitting a multiple linear regression model to explain the response variable Life Expectancy as a function of the regressors: Population; Income; Illiteracy; the Murder rate; the percentage of HS Graduates; Frost, the number of days per year with frost; and Area of the state. Specific meaning and units of measurement of the variables can be obtained with help(state.x77). Using the function lm of R, establish that Life expectancy is essentially a linear function of the predictors Population, Murder, HS Graduates, and Frost. Population and Area are size type variables. Are they better reexpressed in logs?*

```{r comment=NA}
st = data.frame(state.x77)

colnames(st)

# Remove the spaces in column names by
colnames(st)=sapply(colnames(st), function(x)(gsub(" ", "", x)))

plot(st, col = 'pink', pch = 15, 
     cex = 1.1, cex.labels = 1.3, 
     font.labels = 2, gap = 0.15,
     main = 'state.x77'); 
```

Probably we need to transform Area and Population. But first remove those predictors which reveal to be completely non-significant, according to the p-values.

```{r comment=NA}

lm1= lm(Life.Exp ~., data=st)
summary(lm1)

red_lm1 = update(lm1, .~. - Income - Illiteracy - Area)
summary(red_lm1)


st2=st[,c(1,4,5,6,7)]
st2$Population = log(st2$Population); names(st2)[1]=paste('log(Population)')

plot(st2, col = 'blue', pch = 15, 
     cex = 1.1, cex.labels = 1.3, 
     font.labels = 2, gap = 0.15,
     main = 'state.x77'); 

lm2= lm(Life.Exp ~., data=st2)
summary(lm2)

```

In the end the model with the logarithm on Population and the first without it seem to perform almost equally. 

The fact is that the p-value signlas no strongly significance for the predictors Population and Frost, we're going to test a model without them.

```{r comment=NA}
red_lm2 = update(red_lm1, .~. - Population - Frost)
summary(red_lm2)
```

The adj.R-squared is lower than the previous model but we conduct also an ANOVA test.

```{r comment=NA}
anova(red_lm2, red_lm1)
```

Given the low p-value of the F-statistic, we have the final confirmation that we can reject the null-hypothesis, which it is we have to keep Frost and Population as predictors.

Thus our regression model is:

```{r comment=NA}

final_f = Life.Exp ~ log(Population) + Murder + HS.Grad + Frost

final_model = lm(final_f, data=st)

```

```{r comment=NA}

```

