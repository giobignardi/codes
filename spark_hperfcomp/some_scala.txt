//sum of two numbers
def addInt (a: Int, b: Int) : Int = {
var sum:Int = 0
sum = a + b
return sum
}


//factorial of a number
def fact (a: Int) : Int = {
var b:Int = 0
val c:Int =a-1
var factorial:Int = 0 
for (b <- 1 to c){
factorial= a*b
}
return factorial
}



//even
def even (a:Int):Unit = {
var res:Int = 0
def main(args: Array[String]) {
var nPari = Range(0,99,2)
for (i <- 1 to (nPari.length - 1)) {
if (nPari(i) == a) res = 1;
}
println(res)
}
}


//class
class Dog( val name: String, val age: Int){
//
def grow (){
var y: Int = this.age + 1
println(name + y)
}
def is0lder (other: Dog) = {
if (this.age > other.getAge) {
println(name + " " + "is older than" + " " + other.name)
}
}
def getAge() = age
}


//subclass
class Padrone(override val name: String, override val age: Int, val padrone: String) extends Dog(name, age){
var x2: Int = age
override def grow (){
var y2: Int = this.age + 1
println(name + " " + "il cane di"+ " " + " " + padrone + " " + "ora ha" + " " + y2 + "anni")
}
}


//object and classes

object Dog {
def apply(name:String): Dog = new Dog(name,15) //guarda dopo che puoi fare
def apply(age:Int): Dog = new Dog("anonimo",age)
}
val sabbia = Dog("Sabbia") //siccome ho messo String, assegna auto age=15
val x = Dog(10) //siccome ho messo age, assegna "anonimo"
sabbia.is0lder(x)
//NB non ho dovuto mettere new Dog, ma solo Dog


/ iterator #it.hasNext significa che l'elemento ha un successivo #it.next() significa "assume il valore del successivo el dell'iterator"/

def averageAge(dogs: List[Dog]): Double = {   //Dog sta dove starebbe Int etc
var sum = 0
val it = dogs.iterator
while (it.hasNext) {
val dog = it.next()
sum += dog.age
}
return sum / dogs.length
}


//ALCUNI CMD IMPROBABILI
val names = Vector("bob", "fred", "joe", "julia", "kim")

for (name <- names if name.startsWith("j")) //CMD startsWith
println(name)

names.contains("bob") //.contains ->returns a Boolean

names.map(_.toUpperCase) //CMD map

val nNAMES = names.map(_.toUpperCase) //se voglio conservare lo assegno a val

names.flatMap(_.toUpperCase) //CMD flatMap

//altro esempio

val list = List(1,2,3,4,5)
def g(v: Int) = List(v-1, v, v+1)
list.map(x => g(x)) //per ogni elem x della List "list" applica la funz "g"
list.flatMap(x => g(x)) //come sopra e poi butta tt dentro a "list", non tiene separati i risultati in sottoList


//likeJava CMDs : throw new; try; catch; finally

def mydiv(m:Int, n:Int) : Int = {
if (n == 0)
throw new Exception("division by 0") 
else m / n
}
try {
println("5/4 = " + mydiv(5,4))
println("5/0 = " + mydiv(5,0))
println("5/2 = " + mydiv(5,2))
} catch {
case e: Exception => println("Error: " + e.getMessage)
} finally {
println("End")
}
/When an exception is thrown the normal thread of execution is interrupted, and the exception is propagated up the call stack until a catch clause catches it
If you execute a sequence of code that might throw an exception, and you would like to handle that exception, you use the try-catch block. Note that 5/2 isn't executed because after 5/0 the trow Exception is called and the execution jumps to catch.
The finally clause can contain code that you need to be executed, no matter if an exception is thrown or not./

//option can be used to avoid using exceptions
def mydiv(m:Int, n:Int) : Option[Int] = {
if (n == 0)
None
else
Some(m / n)
}


//es factorial
def factorial(m: Int) = {
var fact= 0
def main(args: Array[String]) {
var possFatt = Range(10, 20, 2)
for(i <- 1 to (possFatt.length - 1)){
var x : Int = m%possFatt(i)
if (x == 0) {
var fact = possFatt(i)
println(fact)
}
}
}
}











