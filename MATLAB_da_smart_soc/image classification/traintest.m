% Giovanni Bignardi 100355842

% Attempt to define train and test partition applying hog.m to blocks from images

pics=dir(['/home/gio/codes/da_smart_soc/pics/','/*.jpg']);

% first of all I need a function to divide pics into blocks and apply hog.m
% You can find it defined at the end of the script as appHOG

feats = 300; %number of features per image

xtrain = zeros(floor(0.7*length(pics))*feats,32);
ytrain=[];

for i=1:floor(0.7*length(pics));
	pic = imread(['/home/gio/codes/da_smart_soc/pics/',pics(i).name]);
%assign correct label according to filename
	if strcmp(pics(i).name(1:3),'032');
        label=1; %cartman
    elseif strcmp(pics(i).name(1:3),'051');
        label=2; %galaxy
    elseif strcmp(pics(i).name(1:3),'082');
        label=3; %cowboy-hat
    else
        label=4; %hibiscus
    end 
% now I can store labels into ytrain
	ytrain=[ytrain;label];
% prepare images for appHOG
    pic = im2double(pic);
   	pic = imresize(pic,[320,240]);
% apply appHOG
    xtrain(1+(i-1)*feats:i*feats,:)=appHOG(pic);
end

 

xtest = zeros(floor(0.3*length(pics))*feats,32);
ytest=[];

for i= 1+floor(0.7*length(pics)):floor(length(pics));
	pic = imread(['/home/gio/codes/da_smart_soc/pics/',pics(i).name]);
%assign correct label according to filename
	if strcmp(pics(i).name(1:3),'032');
        label=1; %cartman
    elseif strcmp(pics(i).name(1:3),'051');
        label=2; %galaxy
    elseif strcmp(pics(i).name(1:3),'082');
        label=3; %cowboy-hat
    else
        label=4; %hibiscus
    end 
% now I can store labels into ytest
	ytest=[ytest;label];
% prepare images for appHOG
    pic = im2double(pic);
   	pic = imresize(pic,[320,240]);
% apply appHOG
    xtest(1+(i-1)*feats:i*feats,:)=appHOG(pic);
end


function HOG = appHOG(pic)
% I choose 16x16 as block size and measure how many blocks I can fit in the pic dimensions
	 dim = size(pic);
     nx=floor(dim(1)/(16));
     ny=floor(dim(2)/(16));
     HOG =zeros(nx*ny , 2*2*8);
% then I iterate over the dims to define blocks and apply the provided hog.m func
% results are stored in HOG matrix
     for i=1:nx
         for j=1:ny
             aux=((i-1)*ny)+j;
             %extract the block
             block=pic(1+(i-1)*16:i*16 , 1+(j-1)*16:j*16);
             % apply hog function on the block
             HOG(aux,:)=hog(block, 2, 2, 8);
        end
    end
end
    


