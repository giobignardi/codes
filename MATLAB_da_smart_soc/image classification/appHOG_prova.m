function HOG = appHOG(pic)
% I choose 16x16 as block size and measure how many blocks I can fit in the pic dimensions
	 dim = size(pic)
     nx=floor(dim(1)/(16));
     ny=floor(dim(2)/(16));
     HOG =zeros(nx*ny , 2*2*8);
% then I iterate over the dims to define blocks and apply the provided hog.m func
% results are stored in HOG matrix
     for i=1:nx
         for j=1:ny
             aux=((i-1)*ny)+j;
             %extract the block
             block=pic(1+(i-1)*16:i*16 , 1+(j-1)*16:j*16);
             % apply hog function on the block
             HOG(aux,:)=hog(block, 2, 2, 8);
        end
    end
end
    


