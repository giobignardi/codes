%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Giovanni Bignardi 100355842
% lab2 GMM for speech recognition - Data Analytics for The Smart Society
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK 1 - Implementation of the speaker identification system

for i=1:16,
    
    % Loading of the training files for each speaker
    train = load_train_data('list_train.txt', i);
      
    % Feature extraction
    features_train = melfcc(train,16000, 'wintime', 0.02, 'hoptime', 0.01, 'numcep', 20);
      
    % Speaker GMM models building for each speaker >> GM distributions are
    % stored into the matrix 'models'
    models{i} = fitgmdist(features_train',8,'CovType','diagonal','Replicates',5); %remember that features needs to be inverted with '
    
end 

% Test the model
% Here I can't use the functon load_train_data because the speeches have to
% be extracted one by one, the info about the speaker are stored in a
% separate vector, indexed following the extraction order.

% NOTE: given that list_test1 and list_test2 have the same length, I could
% do the iteration just once but, just in case of future applications with
% test sets of different sizes , I have preferred to keep them separated.

% CLEAN RECORDS
fid = fopen('list_test1.txt'); % list of clean records
info_speech = textscan(fid, '%s%f');
numfich = length(info_speech{1}); % number of speeches
clean_speakers = int16(info_speech{2});    % speaker id of each file
fclose(fid);

predclean_speakers = zeros(numfich,1); %array to store the predicted speakers ids
for i=1:numfich,
    % Read speechs
    speech_test1 = audioread(info_speech{1}{i});
    % Feature extraction
    feat_test1 = melfcc(speech_test1, 16000, 'wintime', 0.02, 'hoptime', 0.01, 'numcep', 20);
    
    % Log-likelihood computation
    for j=1:16,
        LL(j) = sum(log(pdf(models{j},feat_test1'))); %remember that feat_i' needs to be inverted with '
    end
    
    % Define the speaker by MAX log-likelihood
    predclean_speakers(i) = find(LL==max(LL));
end

% NOISY RECORDS
fid = fopen('list_test2.txt'); % list of clean records
info_speech2 = textscan(fid, '%s%f');
numfich2 = length(info_speech2{1}); % number of speeches
noisy_speakers = int16(info_speech2{2});    % speaker id of each file
fclose(fid);

prednoisy_speakers = zeros(numfich2,1); %array to store the predicted speakers ids
for i=1:numfich2,
    
    speech_test2 = audioread(info_speech2{1}{i});
    % Feature extraction
    feat_test2 = melfcc(speech_test2, 16000, 'wintime', 0.02, 'hoptime', 0.01, 'numcep', 20);
    
    % Log-likelihood computation
    for j=1:16,
        LL(j) = sum(log(pdf(models{j},feat_test2'))); %remember that feat_i' needs to be inverted with '
    end
    
    % Define the speaker by max log-likelihood
    prednoisy_speakers(i) = find(LL==max(LL));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK 2 - Evaluation of the baseline sistem

% Here I confront predictions on the speakers with the real speakers IDs I
% have saved reading the test indices.

% Accuracy == percentage of correctly identified test files with respect to the total number of test files

% Clean Recs
correct = 0;
for i=1:numfich,
   if clean_speakers(i) == predclean_speakers(i)
       correct = correct + 1;
   end
end
cleanrec_accuracy = (correct/numfich)*100

% Noisy Recs
correct2 = 0;
for i=1:numfich2,
   if noisy_speakers(i) == prednoisy_speakers(i)
       correct2 = correct2 + 1;
   end
end
noisyrec_accuracy = (correct2/numfich2)*100
      