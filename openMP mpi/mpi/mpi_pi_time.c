#include <stdio.h>
#include <mpi.h> 
#include <time.h>

long num_steps = 100000;
double step = 1.0/100000.0;

int main() {

double start, end;

int i, w_rank, w_size, name_len;
double x, pi, local_sum = 0.0, sum=0.0;
char p_name[MPI_MAX_PROCESSOR_NAME];
double send_vect[num_steps], recv_vect[num_steps]; 

// Initialize the MPI framework.
MPI_Init(NULL, NULL); 

MPI_Barrier(MPI_COMM_WORLD);
start = MPI_Wtime();

// Get the number of processes on the communication group.
MPI_Comm_size(MPI_COMM_WORLD, &w_size); 
// Get the id number of the actual process.
MPI_Comm_rank(MPI_COMM_WORLD, &w_rank);
// Get the name or the actual processor machine.
MPI_Get_processor_name(p_name, &name_len);

if (w_rank ==0){
    int i=0; 
    for (i=0; i<num_steps;i++){
        send_vect[i]=i;
    }
}


// int MPI_Scatter(void *sendbuf, int sendcnt, MPI_Datatype sendtype, void *recvbuf, int recvcnt, MPI_Datatype recvtype, int root, MPI_Comm comm) 
// process id=0 will be the root who scatter the data

MPI_Scatter(send_vect, num_steps/w_size, MPI_DOUBLE, recv_vect, 
num_steps/w_size, MPI_DOUBLE, 0, MPI_COMM_WORLD);

long temp = num_steps/w_size;
for(i = 0; i < temp; i++) {
    x = (recv_vect[i]-0.5)*step;
    local_sum += 4.0/(1.0+x*x);
}

MPI_Barrier(MPI_COMM_WORLD);

//now process id=0 will be the root for receiveing the reduced value

MPI_Reduce(&local_sum, &sum, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

if (w_rank == 0){
    pi = step*sum;
    printf("Process %d (from machine %s) : PI value = %f\n", w_rank, p_name, pi); 
 }

MPI_Barrier(MPI_COMM_WORLD);
end = MPI_Wtime();

// Finalize the MPI framework. 
MPI_Finalize();

if (w_rank == 0) { /* use time on master node */
    printf("Runtime = %f\n", end-start);
}

}


