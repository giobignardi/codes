#include <stdio.h>
#include <mpi.h> 
#include <time.h>

int main() {
FILE * fp;
int txt_size, w_rank, w_size, local_acum=0, acum=0, i, scat;
char buf[1000000];
double start, end;

fp = fopen("pru.txt","r");
fseek(fp, 0L, SEEK_END);
txt_size = ftell(fp);
fseek(fp, 0L, SEEK_SET);
fread (buf,1, txt_size, fp);

// Initialize the MPI framework.
MPI_Init(NULL, NULL); 

MPI_Barrier(MPI_COMM_WORLD);
start = MPI_Wtime();

MPI_Comm_size(MPI_COMM_WORLD, &w_size); 
MPI_Comm_rank(MPI_COMM_WORLD, &w_rank);


//adjust the size of recvbuf
int div = txt_size/w_size;
int remainder = txt_size%w_size;
scat= (txt_size + (w_size - remainder))/w_size;
char recv_vect[scat];

MPI_Scatter (buf, scat, MPI_CHAR, recv_vect, scat, MPI_CHAR, 0, MPI_COMM_WORLD);

for (i=0; i < scat; i++){
    if (recv_vect[i] == ' '){
        int res = 1;
        local_acum = local_acum + res;
    } else {
         int res2 = 0;
         local_acum = local_acum + res2;
    } 
}

MPI_Barrier(MPI_COMM_WORLD); 

MPI_Reduce(&local_acum, &acum, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

if (w_rank == 0){
  printf("The number of blank spaces is %d \n", acum);
}

MPI_Barrier(MPI_COMM_WORLD);
end = MPI_Wtime();

// Finalize the MPI framework. 
MPI_Finalize();

// time on master node.
if (w_rank == 0) { 
    printf("Runtime = %f\n", end-start);
}

}
