#include <stdio.h>
#include <omp.h>
long num_steps = 100000;
double step = 1.0/100000.0;
int main() {
int i;
double x, pi, sum = 0.0;
omp_set_num_threads(omp_get_num_procs());
#pragma omp parallel
{
#pragma omp for private(x) reduction(+:sum)
for(i = 0; i < num_steps; ++i) {
 x = (i-0.5)*step;
 sum += 4.0/(1.0+x*x);
 }
pi = step*sum;
}
printf("PI value = %f\n", pi);
}
