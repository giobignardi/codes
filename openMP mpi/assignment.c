/* The code left as comments into slashes represents how I tried to verify if I was effectively doing a reasonable parallelization. I've also uploaded a CLEANED file.c without comments */ 
#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
int main() {
 FILE * fp;
 int size, i, acum = 0;
 char buf[10000];
 /* read file “pru.txt” and store it in buf[] */
 /* NOTE: file must be smaller than 10000 characters */
 fp = fopen("zeppelin","r");
 fseek(fp, 0L, SEEK_END);
 size = ftell(fp);
 fseek(fp, 0L, SEEK_SET);
 fread (buf,1,size,fp);
 /* printf("%s\n", buf);
 printf("size = %d\n", size); */
 omp_set_num_threads(omp_get_num_procs());
/* add the code to count number of spaces in buf[] */
 #pragma omp parallel
 {
  /*printf("# threads = %d\n", omp_get_num_threads())*/;
  #pragma omp for reduction (+: acum)
  for (i = 0; i<size ; i++ ) {
   if (buf[i] == ' '){
   int res = 1;
   acum = acum + res;
   /*printf("thread and res = %d, %d\n", tid, res); other way to see how many threads I'm working with*/
   }
   else{
   int res2 = 0;
   acum = acum + res2;
   /*printf("thread and res = %d, %d\n", tid, res2); other way to see how many threads I'm working with*/
   }
  }
 }
 /*printf("# procs = %d\n", omp_get_num_procs());*/
 printf("Number of blank spaces = %d\n", acum);
}
