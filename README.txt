# README #

####
HOW TO USE IT

0. git pull origin master

1. git status

2. git add filename.ext  # adds the file to the index

# Once all the files are added, we can commit it. This means that we have finalized
# what additions and/or changes have to be made and they are now ready to be uploaded
# onto our repository. Use the command :

3. git commit -m "some message"

# "some_message" in the above command can be any simple message like 
# "my first commit" or "edit in readme", etc.

# The final step is to push the local repository contents into the remote host
# repository (GitHub), by using the command:

4. git push origin master

#enter the login credentials [usrname and passw]


