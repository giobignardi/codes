setwd('~/poder_municipal/')

######################
library(dplyr)
library(tidyr)
library(stringr)
library(readr)

files <- list.files(path = "CIDAC/", pattern = "*.csv")
files <- paste0("CIDAC/", files)
options(warn=-1)
## set column types in order to be able to bind all files, since each read a different column type
cols = "cccnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn"
elec_mun <- lapply(files, function(x) read_csv(x, col_types = cols))
elec_mun <- as.data.frame(bind_rows(elec_mun))
elec_mun <- tbl_df(elec_mun) %>%
  select(-ANULADOS, -NO.REG.) %>%
  mutate(muncode = gsub("[[:blank:]]", "", CVE_INEGI)) %>%
  separate(CVE_INEGI, c("CVE_ENT", "CVE_MUN"), sep = " ") %>%
  rename(year = `A. Electoral`, PAN = PAN.) %>%
  mutate(CVE_ENT = as.numeric(CVE_ENT), CVE_MUN = as.numeric(CVE_MUN))


############################################################################
# create table from Wiki: List_of_politicians_killed_in_the_Mexican_Drug_War
library(rvest)    
scotusURL <- "https://en.wikipedia.org/wiki/List_of_politicians_killed_in_the_Mexican_Drug_War"
temp <- scotusURL %>% 
  read_html %>%
  html_nodes("table")

table = html_table(temp[1])[[1]]

# subset: 'Position' == 'Municipal president'
table = table[table$Position == 'Municipal president',]

# discard spaces after de, a and la
table$Name = gsub("\\<la\\>\\s+", "la",table$Name)
table$Name = gsub("\\<de\\>\\s+", "de",table$Name)
table$Name = gsub("\\<a\\>\\s+", "a",table$Name)

# split names of the killed majors
library(stringr)
table$name1 = str_split_fixed(table$Name, " ", 4)[,1]
table$name2 = str_split_fixed(table$Name, " ", 4)[,2]
table$name3 = str_split_fixed(table$Name, " ", 4)[,3]
table$name4 = str_split_fixed(table$Name, " ", 4)[,4]

# re-merge names
table$Name = paste(table$name1, table$name2, table$name3, table$name4, sep="")
table$Name = toupper(table$Name)

delete = c('name1','name2','name3','name4', 'Sources')
table = table[,!(names(table) %in% delete)]



################################################################
# load data from https://github.com/andreslajous/poder_municipal
load("allmunicipalities.RData")

# get rid of abbreviated names (G., C., etc)
all_pres_munis$name1 = gsub("\\W*\\b\\w\\b\\W*", "", all_pres_munis$name1)
all_pres_munis$name2 = gsub("\\W*\\b\\w\\b\\W*", "", all_pres_munis$name2)
# merge Names' columns
all_pres_munis$Name <- paste(all_pres_munis$name1, all_pres_munis$name2,
                             all_pres_munis$lastn1, all_pres_munis$lastn2, sep=" ")
all_pres_munis$Name = gsub("\\<NA\\>", "", all_pres_munis$Name)
all_pres_munis$Name = gsub("[[:space:]]", "", all_pres_munis$Name)

merged = merge(all_pres_munis,elec_mun, by.x = c('CVE_ENT','CVE_MUN','year_elec'),
               by.y = c('CVE_ENT','CVE_MUN','year'))

# delete accents
library(stringi)
merged$Name = stri_trans_general(merged$Name, "Latin-ASCII")
table$Name = stri_trans_general(table$Name, "Latin-ASCII")


# It matches all the names correctely except for Enrique Hernandez
for (i in seq(dim(table)[1])){
  #print(which(merged$Name==table$Name[i])) # it matches much less
  print(agrep(table$Name[i],merged$Name, max=0.005, value=TRUE))
}
#hence
for (i in seq(dim(table)[1])){
  merged$Name[agrep(table$Name[i],merged$Name, max=0.005, value=FALSE)] = table$Name[i]
}


final = merge(merged,table, by = 'Name', all.x = TRUE)

# if final$Date != NA the mayor has been assassinated
# BUT we only want the term that a municipal president was killed to have an indicator for assassination.
for (i in seq(dim(final)[1])){
  if (is.na(final$Date[i])){
    final$assassinated[i] = 0
  } else {
    if (word(final$Date[i],-1)==final$year_elec[i]){ # murdered in the year of election
      final$assassinated[i] = 1
    } else if(word(final$Date[i],-1)==tail(strsplit(final$year_finish[i],split='-')[[1]],1)){ # murdered after 1st year of election >> when term ends
      final$assassinated[i] = 1
    } else {
      final$assassinated[i] = 0
    }
  }
}

# municipal presidents not merged >> if you search for them using agrep there is no match
# neither complete nor even partial. and the maximum distance from names allowed is pretty high!
nonmatched = table[which(!(table$Name %in% final$Name)), ]
for (i in seq(dim(nonmatched)[1])){
  #print(which(merged$Name==table$Name[i])) # it matches much less
  print(agrep(nonmatched$Name[i],merged$Name, max=0.01, value=TRUE))
}


pri = final[is.na(final$PRI) == FALSE,]
prd = final[is.na(final$PRD) == FALSE,]
pan = final[is.na(final$PAN) == FALSE,]

# create the running vars
pri[is.na(pri)] = 0
for (i in seq(dim(pri)[1])){
  pri$runVar[i] = pri$PRI[i]-max(pri[i, 20:242], na.rm = TRUE)
}
pri$runVarPercent = pri$runVar/pri$TOTAL

prd[is.na(prd)] = 0
subset = c(19,20, seq(22,242))
for (i in seq(dim(prd)[1])){
  prd$runVar[i] = prd$PRD[i]-max(prd[i, subset], na.rm = TRUE)
}
prd$runVarPercent = prd$runVar/prd$TOTAL

pan[is.na(pan)] = 0
subset = c(19, seq(21,242))
for (i in seq(dim(pan)[1])){
  pan$runVar[i] = pan$PAN[i]-max(pan[i, subset], na.rm = TRUE)
}
pan$runVarPercent = pan$runVar/pan$TOTAL

# save the tables
saveRDS(pri, "pri.Rda")
saveRDS(prd, "prd.Rda")
saveRDS(pan, "pan.Rda")


# glm bionmial regression for the three parties - NO differentiation
#PRI
formula =  assassinated ~ runVarPercent
model = glm(formula, pri, family = binomial(link = 'logit'))
(coef=round(summary(model)$coefficients, d=4))
plot(pri$runVarPercent, pri$assassinated, 
     xlab= 'Running Variable: Difference of Votes(%)', 
     ylab= 'Assassinated yes/no', col='grey', pch=19)

# PRD
formula =  assassinated ~ runVarPercent
model = glm(formula, prd, family = binomial(link = 'logit'))
(coef=round(summary(model)$coefficients, d=4))
plot(prd$runVarPercent, prd$assassinated, 
     xlab= 'Running Variable: Difference of Votes(%)', 
     ylab= 'Assassinated yes/no', col='grey', pch=19)

# PAN
formula =  assassinated ~ runVarPercent
model = glm(formula, pan, family = binomial(link = 'logit'))
(coef=round(summary(model)$coefficients, d=4))
plot(pan$runVarPercent, pan$assassinated, 
     xlab= 'Running Variable: Difference of Votes(%)', 
     ylab= 'Assassinated yes/no', col='grey', pch=19)


######################################################
# Discontinuity Regression
######################################################
library(rdd)
# McCrary sorting test on the running variable
plot(pri$runVarPercent,pri$assassinated)

DCdensity(pri$runVarPercent)
(optimal_bandwidth = IKbandwidth(pri$runVarPercent, pri$assassinated))
formula =  assassinated ~ runVarPercent
rd1 = RDestimate(formula, pri, bw=optimal_bandwidth)
x11()
plot(rd1)
savePlot()
print(rd1)

# McCrary sorting test on the running variable
DCdensity(prd$runVarPercent)
(optimal_bandwidth = IKbandwidth(prd$runVarPercent, prd$assassinated))
formula =  assassinated ~ runVarPercent
rd2 = RDestimate(formula, prd, bw=optimal_bandwidth)
x11()
plot(rd2)
savePlot()
print(rd2)

# McCrary sorting test on the running variable
DCdensity(pan$runVarPercent)
(optimal_bandwidth = IKbandwidth(pan$runVarPercent, pan$assassinated))
formula =  assassinated ~ runVarPercent
rd3 = RDestimate(formula, pan, bw=optimal_bandwidth)
x11()
plot(rd3)
savePlot()
print(rd3)


