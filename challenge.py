from __future__ import division
import pandas as pd
import numpy as np
import os
import subprocess
import csv

# NOTE: THIS SCRIPT IS WRITTEN TO BE LAUNCHED IN THE DIRECTORY WHERE data.txt and layout.txt are stored

layout = pd.read_table("layout.txt")
#1) I read the .txt and iterating row by row (in parallel) I map each one into lists of single elements (letters, numbers or blank spaces)
#2) This way I obtain lists with the same number of bits as the rows in txt
#3) I need to mine 5 values from each row, in order of bit-to-bit appearance: ID,GENDER,AGE,STATE,RACE
#4) Layout gives me the reference of what each group of bit represents:  some are one-hot-econdes for a cathegorical variables, other groups represent a single variable(IDs,age)
#5) I will use layout indications to slice every list and consider only the bits concerning the variable I'm mining
#   - some group of bits (IDs and ages) will be reduced again into a single string
#   - for the 'one-hot-encoded' I search for the appearence of a 1 
#     (Just a note: it can appear max once in each variable-encoding-slice of the list, otherwise there is an error because a cathegorical can't assume two values!)
#     I retrieve which interpretation has that one looking in the layout table and assign the value to the variable

# The auxiliary index
aux = (layout['FIELD POSITION'].values -1).tolist() # -1 because indexing in python starts from zero, while in the table the firs bit is thought as 1

# I need to work on lines in parallel to save time: JOBLIB
# NOte: it's the first time I use this package but it's the only form of parallelization I've found in python other than rely on Spark through pyspark
from joblib import Parallel, delayed
import multiprocessing
     
num_cores = multiprocessing.cpu_count()
count = 0

print 'Compiling users...'

with open('data.txt', 'r') as f:
    reader = csv.reader(f)
    def userminer(row):
        global count
        if count == 1:
            print 'An example of the one-hot-encode slice for STATE'
            print list(row[0])[11:aux[-4]]
        # the first 7 bits are the IDs and they are reduced back into one string
        ID = ''.join(list(row[0])[0:7])
        # bits 8 and 9 are the one-hot-representation for gender
        GENDER = layout['ATTRIBUTE'][layout['FIELD POSITION'] == list(row[0])[7:9].index('1')+8].values[0] if '1' in list(row[0])[7:9] else 'NaN'
        # the 10th and 11th bits have to be reduced into age
        AGE = ''.join(list(row[0])[9:11])
        # bits from the 12th (less the last 4) are the one-hot-representation for states
        STATE = layout['ATTRIBUTE'][layout['FIELD POSITION'] == list(row[0])[11:aux[-4]].index('1')+12].values[0] if '1' in list(row[0])[11:aux[-4]] else 'NaN'
        # the last 4bits of each row are the one-hot-repr for race
        RACE = layout['ATTRIBUTE'][layout['FIELD POSITION'] == list(row[0])[aux[-4]:].index('1')+64].values[0] if '1' in list(row[0])[aux[-4]:] else 'NaN'
        userlist = [ID,GENDER,AGE,STATE,RACE]
        count += 1
        if count%25000==0: #given the long times you can activate this to monitor the process (eventually also multiprocess has a verbose mode)
            print '%s rows mined (%s by each core)' %(count*num_cores, count)
        return userlist
    # here I run the previously defined function in parallel, (1mln/n_cores) rows read by each core (on my quad-core laptop 250,000/core)
    listofusers = Parallel(n_jobs=num_cores)(delayed(userminer)(row) for row in reader)

# I create a DataFrame from the resulting list
users = pd.DataFrame(listofusers, columns=['id','gender','agegroup','state','race'])
# I preferred to add an header to work, even if it won't be saved into the csv (according to the instructions)

# transform the ids and age strings into numbers
users.loc[:,'id'] = users.loc[:,'id'].astype(int)
users.loc[:,'agegroup'] = users.loc[:,'agegroup'].astype(int)
# I could have do it directely extracting them but to avoid complexity and confusion...

# order by IDs
users = users.sort_values('id')

# Readjust agegroup columns according to the ranges in census.csv
users.loc[users.agegroup < 15, 'agegroup']='younger15'
users.loc[(users.agegroup > 14) & (users.agegroup < 18), 'agegroup']='15 to 17 years'
users.loc[(users.agegroup > 17) & (users.agegroup < 22), 'agegroup']='18 to 21 years'
users.loc[(users.agegroup > 21) & (users.agegroup < 25), 'agegroup']='22 to 24 years'
users.loc[(users.agegroup > 24) & (users.agegroup < 30), 'agegroup']='25 to 29 years'
users.loc[(users.agegroup > 29) & (users.agegroup < 35), 'agegroup']='30 to 34 years'
users.loc[(users.agegroup > 34) & (users.agegroup < 40), 'agegroup']='35 to 39 years'
users.loc[(users.agegroup > 39) & (users.agegroup < 45), 'agegroup']='40 to 44 years'
users.loc[(users.agegroup > 44) & (users.agegroup < 50), 'agegroup']='45 to 49 years'
users.loc[(users.agegroup > 49) & (users.agegroup < 55), 'agegroup']='50 to 54 years'
users.loc[(users.agegroup > 54) & (users.agegroup < 60), 'agegroup']='55 to 59 years'
users.loc[(users.agegroup > 59) & (users.agegroup < 65), 'agegroup']='60 to 64 years'
users.loc[(users.agegroup > 64) & (users.agegroup < 70), 'agegroup']='65 to 69 years'

# store it (without header) into a csv in the Data folder. I set index=False, otherwise it would be saved the index as a first column but we don't need it.
users.to_csv('users.csv', sep='|', header=False, index=False)


##########################################################
# SECOND TASK # Fetch and store the relevant census data #
##########################################################
os.makedirs('States')
statenames = list(layout.loc[3:54 ,'ATTRIBUTE']) # list with State names extracted from layout.txt 
#NOTE: I could have extracted statenameso from users.state as well but previously I would have had to check if in users all the states were represented. In the legend for sure they are

# Initially I thought to iterate through statenames and create from there a statecode to add it to the URLs
# for state in statenames:
#   os.makedirs('States/'+statenames[i])
#   statecode = '0'+str(i) if i<=9 else str(i) # ADD A ZERO BEFORE CODE IF IT'S <= 9
# Unfortunately I noticed that on census.ire the codes aren't neither in alphabetical order nor they stretch on a range of values without gaps!
# I thought that I would rather be faster to declare a dictionary than to reorder the statenames from layout

d={'AK':'02', 'AL':'01', 'AR':'05', 'AZ':'04', 'CA':'06', 'CO':'08', 'CT':'09', 'DC':'11', 'DE':'10',
   'FL':'12', 'GA':'13', 'HI': '15', 'IA':'19', 'ID':'16', 'IL':'17', 'IN':'18', 'KS':'20', 'KY':'21',
   'LA':'22', 'MA':'25', 'MD':'24', 'ME':'23', 'MI':'26', 'MN':'27', 'MO':'29', 'MS':'28', 'MT':'30',
   'NC':'37', 'ND':'38', 'NE':'31', 'NH':'33', 'NJ':'34', 'NM':'35', 'NV':'32', 'NY':'36', 'OH':'39',
   'OK':'40', 'OR':'41', 'PA':'42', 'PR':'72', 'RI':'44', 'SC':'45', 'SD':'46', 'TN':'47', 'TX':'48',
   'UT':'49', 'VA':'51', 'VT':'50', 'WA':'53', 'WI':'55', 'WV':'54', 'WY':'56'} # look at page 6-1 (data dictionary) of census.pdf for states abbreviation legend

for statename in statenames:
    os.makedirs('States/'+statename)
    statecode = d[statename]
    os.system("wget -O - http://censusdata.ire.org/"+statecode+"/all_040_in_"+statecode+".P12A.csv | zcat > all_040_in_"+statecode+".P12A.csv")
    os.system("wget -O - http://censusdata.ire.org/"+statecode+"/all_040_in_"+statecode+".P12B.csv | zcat > all_040_in_"+statecode+".P12B.csv")
    os.system("wget -O - http://censusdata.ire.org/"+statecode+"/all_040_in_"+statecode+".P12D.csv | zcat > all_040_in_"+statecode+".P12D.csv")
    os.system("wget -O - http://censusdata.ire.org/"+statecode+"/all_040_in_"+statecode+".P12H.csv | zcat > all_040_in_"+statecode+".P12H.csv")
    os.rename("all_040_in_"+statecode+".P12A.csv","States/"+statename+"/all_040_in_"+statecode+".P12A.csv")
    os.rename("all_040_in_"+statecode+".P12B.csv","States/"+statename+"/all_040_in_"+statecode+".P12B.csv")
    os.rename("all_040_in_"+statecode+".P12D.csv","States/"+statename+"/all_040_in_"+statecode+".P12D.csv")
    os.rename("all_040_in_"+statecode+".P12H.csv","States/"+statename+"/all_040_in_"+statecode+".P12H.csv")


##################################
# THIRD TASK fill census_summary #
##################################

census = pd.read_csv('census_summary.csv')

for state in statenames:
    statecol = pd.Series() # pd.Series to be filled
    for race in ['A','B','H','D']: # start to fill with white, then black, hispanico and finally asian
        auxw = pd.read_csv('States/'+state+'/all_040_in_'+d[state]+'.P12'+race+'.csv').iloc[:,23:].dropna(axis=1)
        # I load the csv for each race, ignore the firs unrelevant 23 columns and drop the following null ones
        # I refer to census.pdf, cap.6, pag.36 for columns interpretation (some had to be combined)
        M = auxw.iloc[:,0].append([(auxw.iloc[:,1]+auxw.iloc[:,2]+auxw.iloc[:,3]),auxw.iloc[:,4],
                                   auxw.iloc[:,5],auxw.iloc[:,6],auxw.iloc[:,7],auxw.iloc[:,8],
                                   auxw.iloc[:,9],auxw.iloc[:,10],auxw.iloc[:,11],(auxw.iloc[:,12]+auxw.iloc[:,13]),
                                   (auxw.iloc[:,14]+auxw.iloc[:,15])])
        F = auxw.iloc[:,24].append([(auxw.iloc[:,25]+auxw.iloc[:,26]+auxw.iloc[:,27]),auxw.iloc[:,28],
                                   auxw.iloc[:,29],auxw.iloc[:,30],auxw.iloc[:,31],auxw.iloc[:,32],
                                   auxw.iloc[:,33],auxw.iloc[:,34],auxw.iloc[:,35],(auxw.iloc[:,36]+auxw.iloc[:,37]),
                                   (auxw.iloc[:,38]+auxw.iloc[:,39])])
        statecol = statecol.append([M,F]) #append male and females by age (for this race) to the pd.Series
    census.loc[:,state] = statecol.values #assign the values to the proper column

# finally I fill the tot columns using the convenient pandas.DataFrame.sum() function
census.loc[:,'tot']=census.sum(axis=1)

# Save into csv with header (instruction don't say anything for what concerns the question header/None-header)
census.to_csv('filled_census_summary.csv', sep='|', index=False)



##############
# FINAL TASK #
##############

census = pd.read_csv('filled_census_summary.csv',sep='|')
users = pd.read_csv('users.csv', sep='|', header=None )
users.rename(columns={0:'id',1:'gender',2:'agegroup',3:'state',4:'race'}, inplace=True)

# Problem: ids gender race and agegroup are all filled BUT I have users without information about the state they belong at
# Is there a way to infer their nationality?
# >> If I order the rows of users by state, I could eventually search for a pattern in the first figures of IDs
# Something like what once was for the licence plates, whose first two letters indicated the province 
# where the car was registered. Let's see!
# The results don't suggest a pattern and searching some more I definitely infer that there aren't
# I have to follow another way
# LOOK into the .html otuput of the jupyter-notebook with python kernel for more information about my insight

#############################################################################################################

# Interesting! There is a Nationless ID for each AgeGroup (and for all the 4 races and both 2 genders)
# You can find a barplot into the .html
# Why is this interesting?
# In our 1mln sample a few segments of the census' counts by gender & ageGroup & race & state are not represented,
# BUT this only happens because the State information is missing!

# Hence, my strategy will be:
# 1) iterate FOR gender FOR race and FOR agegroup
#   >> SUBSET users BY gender & race & agegroup
# agegroup is the last because I considered it the more susceptible to have missing records (even if we verified it hasn't, I followed a logic order)
#   >> retrieve census slice for each subset, like:
#   >> census_row = census.loc[census.Gender==gender,:].loc[census.Race==race,:].loc[census.loc[:,'Age group']==agegroup]
# 2) iterate FOR state 
#   >> assign to the ids with a certain nationality a weight 
#   >> weight = StatePopulation in the census slice / number of ids with that nationality
# for some census slices there will be some States not appearing in the subset of our users
# this is why I store the StatePopulations of the State for which weights are computed in an accumulator
# at the end of the "FOR state" iteration I can confront the total of population (last column) with the value in acum
# if there is a difference, IT WILL BE SHARED BY USERS WITH NO STATE!
# Note: it's true there won't be always a landless user, but we have seen that there are for each age group which is the most
# copious variable (other than states, obviously). Hence, most of the time, we will encounter some landless id 
# to whom we can assign a weight to stand for the unrepresented share of the slice population.

print 'Computing weights...'

weights = pd.DataFrame(columns=['id','weight'])

for gender in users.gender.unique():
    for race in users.race.unique():
        for agegroup in  np.delete(users.agegroup.unique(),np.argwhere(users.agegroup.unique()=='younger15')): # Delete the agegroup 'younger15', which doesn't appear in census (it starts from 15-17 years old)
            # census slice
            census_row = census.loc[census.Gender==gender,:].loc[census.Race==race,:].loc[census.loc[:,'Age group']==agegroup]
            # if the subset contains a certain agegroup -> proceed
            if agegroup in users.loc[users.gender==gender,:].loc[users.race==race,:].agegroup.unique():
                # in fact this is unseful according to the previous statement, saying that in our sample
                # segment for each gender,race and agegroup always exist, but given that agegroup could 
                # be a variable susceptible of missing values I find usegul to add this if condition 
                # >>> subset for agegroup
                subset= users.loc[users.gender==gender,:].loc[users.race==race,:].loc[users.agegroup==agegroup,:]
                # count the number of users per state in the subset
                id_per_state = subset.loc[:,'state'].value_counts()
                # an accumulator to add the quote of total population covered by the segments in the subset
                acum_pop_ref = 0
                # iterate over states in the subset
                for state in np.delete(subset.state.unique(), np.argwhere(pd.isnull(subset.state.unique()))): # statenames - nan
                    # value for that Gender&Race&Age&State segment in census
                    pop_ref = census_row[state].values[0]
                    # add weights to weight column
                    subset.loc[subset.state==state,'weight'] = pop_ref/id_per_state[state]
                    # add the census population for that state in the acum
                    acum_pop_ref += pop_ref
                # when the loop finish, I count the number of cells with nan State in the subset
                num_nanState = subset.loc[subset.isnull().any(axis=1)].shape[0]
                totPopRow = census_row['tot'].values[0] # total population of the Gender & Race & Age slice (sum of all states pop)
                # the weight assigned to the ids with nan State covers the gap between the tot pop and the sum in acum
                subset.loc[subset.isnull().any(axis=1),'weight'] = (totPopRow - acum_pop_ref)/num_nanState
                # append the weights for all the Gender & Race & Age subset to weights
                weights = weights.append([subset.loc[:,['id','weight']]])
            else:
                continue

weights.to_csv('weights.csv', sep='|', header=False, index=False)

print 'A benchmark to estimate my representation accuracy \n'
print "weights['weight'].sum()/10**6 = %s \n" %(weights['weight'].sum()/10**6)
print "census['tot'].sum()/10**6 = %s \n" %(census['tot'].sum()/10**6)
print 'Quite satisfying considering the dimension of our sample confronted to the U.S. census population \n'
print 'THANK YOU, HOPE TO HEAR BACK FROM YOU'
